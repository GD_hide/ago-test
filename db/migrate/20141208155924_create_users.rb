class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.integer :gender
      t.belongs_to :team
      t.timestamps
    end
  end
end
