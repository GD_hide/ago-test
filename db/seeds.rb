# On créer 1000 équipes
1000.times do |index|
  Team.create(name: "Team #{index}")  
end

# On ajoute un membre par équipe
1000.times do |index|
  User.create(username: "User #{index}", team_id: Team.includes(:users).where(users: { id: nil }).pluck(:id).sample)
end

# On ajoute des membres dans des équipes de façon aléatoire
1500.times do |index|
  User.create(username: "User #{index}", team_id: Team.pluck(:id).sample)
end

# On créer des rendus de type aléatoire associés à des équipes
3000.times do
  Asset.create(team_id: Team.pluck(:id).sample, kind: ['image', 'file'].sample)
end
