class Asset < ActiveRecord::Base
  belongs_to :team

  after_save :update_counter_cache
  after_destroy :update_counter_cache

  def update_counter_cache
    self.team.image_assets_count = Asset.where("team_id = #{self.team_id} AND kind = 'image'").count   
    self.team.file_assets_count  = Asset.where("team_id = #{self.team_id} AND kind = 'file'").count
    self.team.save!
  end


end
