## Temps maximum : 2 heures 

# Prérequis : 
Forker le repo puis executer les seeds

# Etape 1 (30 min)
Suite à une demande très urgente d'un client, nous cherchons à afficher une page avec la liste des équipes. L'objectif de cette étape est de satisfaire très rapidement le besoin.

La page devra afficher le nom de chaque équipes ansi que pour chacune d'entre elle :
  - le nombre de fichier envoyés de type (kind) "file"
  - le nombre de fichier envoyés de type (kind) "image"
  - le nombre d'utilisateurs dans l'équipe ansi que leurs pseudo (username).

Votre code doit afficher la liste des équipes par nombre de membres décroissant. En voici un exemple :

```
MyAmazingTeam : 5 images, 0 files, 4 users (User 1, User 2, User 3, User 4)
AnotherTeam : 3 images, 8 files, 2 users (User 5, User 6)
ANewTeam : 0 images, 0 files, 1 users (User 7)
...
```

Cette étape doit être codée très rapidement et sortir cette liste sans se soucier des performances. Merci de commiter à la fin de cette étape.

# Etape 2 (1h30)

L'étape 1 est finie ? Attaquons la suite : nous cherchons maintenant à afficher la même page de façon plus performante, l'idée est de diminuer au maximum le nombre de requêtes à la base de données.
