class AddCountersToTeam < ActiveRecord::Migration
  
  def change
    add_column :teams, :users_count, :integer, default: 0
    add_column :teams, :image_assets_count, :integer, default: 0
    add_column :teams, :file_assets_count, :integer, default: 0

    Team.reset_column_information
  
    Team.all.each do |p|
      Team.update_counters p.id, users_count: p.users.length
      p.update_attribute image_assets_count: p.assets.where(kind: "image").length
      p.update_attribute file_assets_count: p.assets.where(kind: "file").length
    end

  end
end
