class TeamsController < ApplicationController
  def index
    @teams = Team.includes(:users).all
  end
end
