class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.integer :team_id
      t.string :kind

      t.timestamps
    end
  end
end
